# haqler
# Tue Mar 26 03:51:15 EDT 2019


import re
import json
import argparse
import requests
import logging
from google.cloud import storage


def getAhsayGetBackupSetConf(BackupServer, sysUser, sysPwd, loginName, backupSetId):
    buckets = []
    for bs_id in backupSetId:
        url = "http://" + BackupServer + "/obs/api/json/GetBackupSet.do"
        data = {'SysUser': sysUser, 'SysPwd': sysPwd, 'LoginName': loginName, 'BackupSetID': bs_id}
        response = requests.post(url, data=data, timeout=5)

        if (response.ok):
            jData = json.loads(response.content.decode('utf-8'))
            if 'Destination' in jData['Data']['ApplicationSettings']:
                buckets.append(jData['Data']['ApplicationSettings']['Destination'].get('TopDir'))

        else:
            response.raise_for_status()

    return buckets


def getAhsayGetReplMode(BackupServer, sysUser, sysPwd):
    url = "http://" + BackupServer + "/obs/api/json/GetReplicationMode.do"
    data = {'SysUser': sysUser, 'SysPwd': sysPwd}
    response = requests.post(url, data=data, timeout=5)

    if (response.ok):
        jData = json.loads(response.content.decode('utf-8'))
        print(jData)

    else:
        response.raise_for_status()

    return 0


def getAhsayGetSystemSetting(BackupServer, sysUser, sysPwd):
    url = "http://" + BackupServer + "/obs/api/json/2/GetSystemSetting.do"
    data = {'SysUser': sysUser, 'SysPwd': sysPwd}
    response = requests.post(url, data=data, timeout=5)

    if (response.ok):
        jData = json.loads(response.content.decode('utf-8'))
        print('system settings')
        print(jData)

    else:
        response.raise_for_status()

    return 0


def getAhsayListPolicyGroups(BackupServer, sysUser, sysPwd):
    url = "http://" + BackupServer + "/obs/api/json/2/ListPolicyGroups.do"
    data = {'SysUser': sysUser, 'SysPwd': sysPwd, 'LoginName': 'qowa'}
    response = requests.post(url, data=data, timeout=10)

    if (response.ok):
        jData = json.loads(response.content.decode('utf-8'))
        print('policy groups')
        print(json.dumps(jData, indent=4))

    else:
        response.raise_for_status()

    return 0


def getAhsayListUsersName(BackupServer, sysUser, sysPwd):
    users = []
    url = "http://" + BackupServer + "/obs/api/json/2/ListUsers.do"
    data = {'SysUser': sysUser, 'SysPwd': sysPwd, 'LoginNameOnly': 'true'}
    response = requests.post(url, data=data, timeout=5)

    if (response.ok):
        jData = json.loads(response.content.decode('utf-8'))
        if 'User' in jData:
            for usr in jData['User']:
                users.append(usr['LoginName'])

    else:
        response.raise_for_status()

    return users


def getAhsayListUsers(BackupServer, sysUser, sysPwd):
    users = []
    url = "http://" + BackupServer + "/obs/api/json/2/ListUsers.do"
    data = {'SysUser': sysUser, 'SysPwd': sysPwd, 'SearchStr': 'qowa'}
    response = requests.post(url, data=data, timeout=5)

    if (response.ok):
        jData = json.loads(response.content.decode('utf-8'))
        print(json.dumps(jData, indent=4))

    else:
        response.raise_for_status()

    print(users)
    return users


def getGCSUsersList(bucket, server_path):
    if server_path[-1] != '/':
        server_path += '/'

    userNamePos = server_path.count('/')
    logging.info('Pos: ' + str(userNamePos))
    reSystemChars = '.*[\$].*'   # Add new one if need. Use '\' to preserve some chars.
    blobs = bucket.list_blobs(prefix=server_path)

    gcs_users = []
    for blob in blobs: 
        logging.info('File path: ' + blob.name)
        user_name = blob.name.split('/')[userNamePos]
        logging.info('User name: ' + user_name)
        if not re.match(reSystemChars, user_name):
            gcs_users.append(user_name)

    return set(gcs_users)


def getAhsayListBackupSets(BackupServer, sysUser, sysPwd, loginName):
    url = "http://" + BackupServer + "/obs/api/json/ListBackupSets.do"
    data = {'SysUser': sysUser, 'SysPwd': sysPwd, 'LoginName': loginName}
    response = requests.post(url, data=data, timeout=5)

    backupsets_id = []
    if (response.ok):
        jData = json.loads(response.content.decode('utf-8'))
        for backupset in jData.get('Data'):
           backupsets_id.append(backupset['ID']) 

    else:
        response.raise_for_status()

    return backupsets_id


def getGCSBucketObjList(prefix=''):
    storage_client = storage.Client.from_service_account_json('./PruebaServiceKey.json')
    buckets = storage_client.list_buckets(prefix)

    return buckets


def getGCSBucketObjByName(bucket_name):
    storage_client = storage.Client.from_service_account_json('./PruebaServiceKey.json')
    bucket = storage_client.get_bucket(bucket_name)

    return bucket


def getGCSBucketNameList(bucketObjList):
    bucket_id_list = []
    for bucket in bucketObjList:
        bucket_id_list.append(bucket.id)

    return bucket_id_list


def check_ip_addr(ip_value):
    if re.match("^([0-9]{1,3}.){3}[0-9]{1,3}$", ip_value) is None:
        msg = "%r is not valid IP address" % ip_value
        raise argparse.ArgumentTypeError(msg)

    return ip_value


def process_user_args():
    parser = argparse.ArgumentParser(usage='%(prog)s [options]')
    parser.add_argument('--ahsay-server', required=True, help='Server IP of an AhsayCBS system', type=check_ip_addr)
    parser.add_argument('--ahsay-sysuser', required=True, help='Username of an AhsayCBS system user with admin role')
    parser.add_argument('--ahsay-syspass', required=True, help='Password of an AhsayCBS system user with admin role')
    parser.add_argument('--ahsay-server-path', required=True, help='Server path to the user folders in the GCS')
    parser.add_argument('--gcs-bucket-name', required=True, help='GCS bucket name')
    args = parser.parse_args()

    print('Ahsay server: ' + args.ahsay_server)
    print('Ahsay admin user: ' + args.ahsay_sysuser)
    print('Ahsay admin pass: ' + args.ahsay_syspass)
    print('Ahsay server path to user folders: ' + args.ahsay_server_path)
    print('Google Cloud bucket name: ' + args.gcs_bucket_name)

    return args

logging.basicConfig(level=logging.INFO, filename='backup_stat.log', filemode='w', format='%(name)s - %(asctime)s - %(message)s')
logging.info('Start logging')

userArgs = process_user_args()
logging.info('Server path: ' + userArgs.ahsay_server_path)

ahsay_users_list = getAhsayListUsersName(userArgs.ahsay_server, userArgs.ahsay_sysuser, userArgs.ahsay_syspass)
#print(ahsay_users_list)
logging.info(ahsay_users_list)

gcs_bucket = getGCSBucketObjByName(userArgs.gcs_bucket_name)
#print(gcs_bucket)
logging.info(gcs_bucket)

gcs_users_list = getGCSUsersList(gcs_bucket, userArgs.ahsay_server_path)
#print(gcs_users_list)
logging.info(gcs_users_list)


print('\n*** All Ahsay users ***')
for user in ahsay_users_list:
    print('\t' + user)


print('\n*** All user folders in directory ' + userArgs.ahsay_server_path + ' ***')
for user in gcs_users_list:
    print('\t' + user)


print("\n*** Ahsay users that don't have folder in bucket " + userArgs.gcs_bucket_name + " ***")
for user in ahsay_users_list:
    if user not in gcs_users_list: 
        print('\t' + user)


print("\n*** Folders in bucket " + userArgs.gcs_bucket_name + " that don't have corresponding user in Ahsay server ***")
for user in gcs_users_list:
    if user not in ahsay_users_list: 
        print('\t' + user)

#getAhsayListUsers(userArgs.ahsay_server, userArgs.ahsay_sysuser, userArgs.ahsay_syspass)
#getAhsayGetReplMode(userArgs.ahsay_server, userArgs.ahsay_sysuser, userArgs.ahsay_syspass)
#getAhsayGetSystemSetting(userArgs.ahsay_server, userArgs.ahsay_sysuser, userArgs.ahsay_syspass)
#getAhsayListPolicyGroups(userArgs.ahsay_server, userArgs.ahsay_sysuser, userArgs.ahsay_syspass)
#ahsay_backupsets = getAhsayListBackupSets(userArgs.ahsay_server, userArgs.ahsay_sysuser, userArgs.ahsay_syspass, userArgs.ahsay_login)
#ahsay_buckets = getAhsayGetBackupSetConf(userArgs.ahsay_server, userArgs.ahsay_sysuser, userArgs.ahsay_syspass, userArgs.ahsay_login, ahsay_backupsets)
#
#print('\n***Google buckets***')
#for i in gcs_buckets:
#    print('\t' + i)
#
#print('\n***Ahsay buckets***')
#for i in ahsay_buckets:
#    print('\t' + i)
#
#print('\n***GCS buckets that doesn\'t exist in Ahsay***')
#for bucket in gcs_buckets:
#    if not bucket in ahsay_buckets:
#        print('\t' + bucket)
#
