**Requirements**
--

    Python3
    
    Pip

Run script **setup-env.sh** to load python modules `google-cloud-storage` and `request`.


**Usage:**
--

**backup_stat.py [options]**

optional arguments:

  **-h, --help**            show this help message and exit
  
  **--ahsay-server AHSAY_SERVER** - Server IP of an AhsayCBS system
                        
  **--ahsay-sysuser AHSAY_SYSUSER** - Username of an AhsayCBS system user with admin role
                        
  **--ahsay-syspass AHSAY_SYSPASS** - Password of an AhsayCBS system user with admin role
                        
  **--ahsay-login AHSAY_LOGIN** - Client user name


**Usage example**
--

python backup_stat.py --ahsay-server **ip_addr** --ahsay-sysuser **admin_user** --ahsay-syspass **admin_pass** --ahsay-login **backup_user**

